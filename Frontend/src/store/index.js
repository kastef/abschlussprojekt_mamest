import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store( {

    state: {
        climber: null,

        genders: null,

        countries: null,

        cragTypes: null,

        crags: [],

        routes: [],

        difficulties: null,

        welcomePageText: true,

        /* Filter */
        routesByCountry: null,
        routesByCragType: null,
        routesByDifficulty: null,
        /* Filter */

        /* CragOverview */
        cragsByCountry: [],
        routesByCrag: null,
        /* CragOverview */

        cragNames: [],

        selectedCrag: null,

        routeName: [],

        comments: [],

        otherComments: [],

        commentsByRoute:[],

        cragName: [],

        sendComment: [],

        signUps: [],

        signUpsByCrag: [],

        climberSignUps: [],

        finishedSignUps: [],
        
        routeId: [],
    },

    mutations: {
        setClimber(state, climber) {
            state.climber = climber;
        },
        setCragById(state, selectedCrag) {
            state.selectedCrag = selectedCrag;
        },
        setCountries(state, countries) {
            state.countries = countries;
        },
        setCragTypes(state, cragTypes) {
            state.cragTypes = cragTypes;
        },
        setCrags(state, crags) {
            state.crags = crags;
        },
        setRoutes(state, routes) {
            state.routes = routes;
        },
        setRoutesByCountry(state, routesByCountry) {
            state.routesByCountry = routesByCountry;
        },
        setRoutesByCragType(state, routesByCragType) {
            state.routesByCragType = routesByCragType;
        },
        setRoutesByDifficulty(state, routesByDifficulty) {
            state.routesByDifficulty = routesByDifficulty;
        },
        setDifficulties(state, difficulties) {
            state.difficulties = difficulties;
        },
        setCragNames(state, cragNames) {
            state.cragNames = cragNames;
        },
        setCragsByCountry(state, cragsByCountry) {
            state.cragsByCountry = cragsByCountry
        },
        setRoutesByCragId(state, routesByCrag) {
            state.routesByCrag = routesByCrag;
        },
        setRouteName(state,routeName)
        {
            state.routeName = routeName
        },
        setGenders(state, genders) {
            state.genders = genders
        },
        setComments(state,comment){
            state.comments = comment;
        },
        setOtherComments(state,otherComments){
            state.otherComments = otherComments;
        },
        setSendComment(state,sendComment){
            state.sendComment = sendComment;
        },
        setCommentsByRoute(state, commentsByRoute) {
            state.commentsByRoute = commentsByRoute
        },
        setAllSignUps(state, signUps) {
            state.signUps = signUps
        },
        setSignUpsByCrag(state, signUpsByCrag) {
            state.signUpsByCrag = signUpsByCrag
        },
        setClimberSignUps(state, climberSignUps){
            state.climberSignUps = climberSignUps
        },
        setCrag(state,cragName){
            state.cragName = cragName;
        },
        deleteClimberSignUp(state, deleteSignUp){
            state.climberSignUps = deleteSignUp
        },
        welcomePageTextChangerFalse(state){
            state.welcomePageText = false
        },
        welcomePageTextChangerTrue(state){
            state.welcomePageText = true
        },
        putClimberSignUp(state, putSignUp){
            state.climberSignUps = putSignUp
        },
        setFinishedSignUps(state, finishedSignUps){
            state.finishedSignUps = finishedSignUps
        }
    },

    actions: {
        /* USABILITY SET UP */
        async login(context, credentials) {
            var basicAuth = 'Basic ' + btoa(credentials.email + ':' + credentials.password)
            const response = await Vue.axios.get('/api/login', {
                headers: { 'Authorization': basicAuth }
            });
            context.commit('setClimber', response.data);
        },
        async register(context, newClimberData) {
            const response = await Vue.axios.post('/api/register', newClimberData);
            context.commit('setClimber', response.data)
        },
        async loadLoggedInClimber (context) {
            const response = await Vue.axios.get('/api/activeClimber');
            context.commit('setClimber', response.data);
        },
        async logout(context) {
            await Vue.axios.post( '/api/logout');
            context.commit('setClimber', null);
        },
        async loadGenders(context) {
            const response = await Vue.axios.get('/api/genders' );
            context.commit('setGenders', response.data);
        },
        async deleteClimber(context, climberId) {
            const response = await Vue.axios.delete('/api/deleteClimber/' + climberId);
            console.log(response.data);
            context.commit('setClimber', null)
        },
        async editClimber(context, editedClimber) {
            console.log(editedClimber)
            const response = await Vue.axios.put('/api/editClimber', editedClimber);
            console.log(response.data)
            context.commit('setClimber', response.data)
        },
        async editAvatar(context, avatar) {
            const response = await Vue.axios.put('/api/editAvatar', avatar);
            context.commit('setClimber', response.data)
        },

        /* COUNTRY - CRAG - ROUTES */
        async loadCountries(context) {
            const response = await Vue.axios.get('/api/allCountries');
            context.commit('setCountries', response.data);
        },
        async loadCragTypes(context) {
            const response = await Vue.axios.get('/api/cragtypes');
            context.commit('setCragTypes', response.data);
        },
        async loadCrags(context) {
            const response = await Vue.axios.get('/api/allCrags');
            context.commit('setCrags', response.data);
        },
        async loadRoutes(context) {
            const response = await Vue.axios.get('/api/allRoutes');
            context.commit('setRoutes', response.data);
        },
        async loadDifficulties(context) {
            const response = await Vue.axios.get('/api/grading');
            context.commit('setDifficulties', response.data);
        },

        async loadCragNames(context) {
            const response = await Vue.axios.get('/api/allCragNames');
            context.commit('setCragNames', response.data)
        },
        async loadCragsByCountry(context, countryName) {
            const response = await Vue.axios.get('/api/cragsByCountry/' + countryName);
            context.commit('setCragsByCountry', response.data)
        },
        async loadCragById(context, selectedCragId) {
            const response = await Vue.axios.get('api/cragById/' + selectedCragId);
            context.commit('setCragById', response.data)
        },
        async loadRoutesByCragId(context, selectedCragId) {
            const response = await Vue.axios.get('/api/allRoutesFromCrag/' + selectedCragId);
            context.commit('setRoutesByCragId', response.data)
        },

        async getRoutesWithName(context, credentials) {
            const response = await Vue.axios.post('/api/routes', credentials);
            context.commit('setRouteName',response.data);

        },

        /* Filter */
        async loadRoutesByCountryName(context, selectedCountryName) {
            const response = await Vue.axios.get('/api/routesByCountryName/'+ selectedCountryName);

            context.commit('setRoutesByCountry', response.data);
        },
        async loadRoutesByCragType(context, selectedCragType) {
            const response = await Vue.axios.get('/api/routesByCragType/'+ selectedCragType);
            context.commit('setRoutesByCragType', response.data);
        },
        async loadRoutesByDifficulty(context, selectedDifficulty) {
            const response = await Vue.axios.get('/api/routesByDifficulty/'+ selectedDifficulty);
            context.commit('setRoutesByDifficulty', response.data);
        },


        /* async getComment(context){
              const response = await Vue.axios.get('/api/signUpsByClimberIdAndRouteId/1/123')  // /1/123 muss mit /climberId/routeId ersetzt werden
              context.commit('setComments',response.data);
              console.log(response.data);
          },*/
        async getOtherComments(context,routeId){
            const response = await Vue.axios.get('/api/signUpsByRouteId/'+routeId)
            context.commit('setOtherComments',response.data)
        },
        async sendComment(context,{signUp,textfield}){ 
            const response = await Vue.axios.post("/api/sendComment/"+signUp , textfield)
            context.commit('setSendComment',response.data)
        },

        /* SIGN UPS */
        async loadAllSignUps(context){
            const response = await Vue.axios.get('/api/allSignUps');
            context.commit('setAllSignUps', response.data);
        },

        async loadSignUpsByCrag(context, cragId){
            const response = await Vue.axios.get('/api/signUpsByCrag/' + cragId);
            context.commit('setSignUpsByCrag', response.data);
        },

        async loadClimberSignUps(context, activeClimberId){
            const response = await Vue.axios.get('/api/signUpsByClimberId/' + activeClimberId);
            context.commit('setClimberSignUps', response.data);
        },
        async loadCommentsByRoute(context,routeId) {
            const response = await Vue.axios.get('/api/commentsByRoute/'+routeId)
            context.commit('setCommentsByRoute', response.data)
        },

        async addSignUp(context, collectedIds) {
            const response = await Vue.axios.post('/api/createSignUp', collectedIds);
            console.log(response.data);
            //context.commit('setAllSignUps', response.data);
        },
        async getCragSpecs(context,cragName){
            const response = await Vue.axios.get('/api/cragSpecs/'+ cragName);
            console.log("Crag Specs:" + response.data);
            console.log(response.data+" "+response+" "+cragName);
            context.commit('setCrag' , response.data);
        },
        async deleteClimberSignUps(context, signUpId){
            const response = await Vue.axios.delete('/api/deleteSignUp/' + signUpId);
            context.commit('deleteClimberSignUp', response.data);
        },
        async welcomePageTextFalse(context){
            context.commit('welcomePageTextChangerFalse')
        },
        async welcomePageTextTrue(context){
            context.commit('welcomePageTextChangerTrue')
        },
        /*
        async putClimberSignUps(context, signUpId){
            const response = await Vue.axios.put('/api/putFinishedDate/' + signUpId)
            context.commit('putClimberSignUp', response.data)
        }
        */
        async putClimberSignUps(context, dateData){
            const response = await Vue.axios.put('/api/putFinishedDate', dateData)
            context.commit('putClimberSignUp', response.data)
        },
        async loadHallOfFame(context, routeId){
            console.log("im store");
            const response = await Vue.axios.get('/api/signUpsByRouteIdAndFinishedDate/' + routeId)
            console.log(response.data);
            context.commit('setFinishedSignUps', response.data)
        }
    }
})
