import VueRouter from "vue-router"
import Vue from "vue"
import LandingPage from "../views/LandingPage.vue"
import LoginPage from "../views/LoginPage";
import RegisterPage from "../views/RegisterPage";
import WelcomePage from "../views/WelcomePage.vue"
import Profile from "../views/Profile.vue";
import EditPage from "../views/EditPage.vue";
import CragOverview from "../views/CragOverview";
import RouteOverview from "../views/RouteOverview";
import ProjectOverview from "../views/ProjectOverview"
import PostFeed from "../views/PostFeed.vue"
import WeatherPage from "../views/WeatherPage"
import AboutUs from "../views/AboutUs";
import store from "../store/"
import HallOfFame from "../views/HallOfFame"
import AvatarEditPage from "../views/AvatarEditPage";

Vue.use(VueRouter)

const routes = [
    {
        path: "/",
        name: "LandingPage",
        component: LandingPage
    },
    {
        path: "/login",
        name: "LoginPage",
        component: LoginPage
    },
    {
        path: "/register",
        name: "RegisterPage",
        component: RegisterPage
    },
    {
        path: "/welcomePage",
        name: "WelcomePage",
        component: WelcomePage,
        beforeEnter: (to, from, next) => {
            if (store.state.climber) {
                next()
            } else {
                alert('Login to proceed')
                next('/login')
            }
        }
    },
    {
        path:'/profile',
        name:'Profile',
        component: Profile,
        beforeEnter: (to, from, next) => {
            if (store.state.climber) {
                next()
            } else {
                alert('Login to proceed')
                next('/login')
            }
        }
    },
    {
        path:'/editPage',
        name:'EditPage',
        component: EditPage,
        beforeEnter: (to, from, next) => {
            if (store.state.climber) {
                next()
            } else {
                alert('Login to proceed')
                next('/login')
            }
        }
    },
    {
        path:'/avatarEditPage',
        name:'AvatarEditPage',
        component: AvatarEditPage,
        beforeEnter: (to, from, next) => {
            if (store.state.climber) {
                next()
            } else {
                alert('Login to proceed')
                next('/login')
            }
        }
    },


    {
        path:'/cragOverview',
        name:'CragOverview',
        component: CragOverview,
        beforeEnter: (to, from, next) => {
             if (store.state.climber) {
                 next()
             } else {
                 alert('Login to proceed')
                 next('/login')
             }
         }
    },
    {
        path:'/routeOverview',
        name:'RouteOverview',
        component: RouteOverview,
        beforeEnter: (to, from, next) => {
             if (store.state.climber && ( !(store.state.selectedCrag===null) || (store.state.selectedCrag===[]) ) ) {
                 next()
             } else if ( ( (store.state.selectedCrag===null) || (store.state.selectedCrag===[]) ) ) {
                next('/cragOverview')
            } else {
                alert('Login to proceed')
                next('/login')
             }
         }
    },
    {
        path: "/projectOverview",
        name: "ProjectOverview",
        component: ProjectOverview,
        beforeEnter: (to, from, next) => {
            if (store.state.climber) {
                next()
            } else {
                alert('Login to proceed')
                next('/login')
            }
        }
    },
    {
        path:'/postFeed/:routeid',
        name:'PostFeed',
        component: PostFeed,
        beforeEnter: (to, from, next) => {
            if (store.state.climber) {
                next()
            } else {
                alert('Login to proceed')
                next('/login')
            }
        }
    },
    {
        path:'/weather',
        name:'WeatherPage',
        component: WeatherPage,
        beforeEnter: (to, from, next) => {
            if (store.state.climber) {
                next()
            } else {
                alert('Login to proceed')
                next('/login')
            }
        }
    },
    {
        path:'/aboutUs',
        name:'AboutUs',
        component: AboutUs,
        beforeEnter: (to, from, next) => {
             if (store.state.climber) {
                 next()
             } else {
                 alert('Login to proceed')
                 next('/login')
             }
         }
    },
    {
        path:'/hallOfFame',
        name:'HallOfFame',
        component: HallOfFame,
        beforeEnter: (to, from, next) => {
            if (store.state.climber && store.state.finishedSignUps.length) {
                next()
            } else if (!store.state.finishedSignUps.length) { //( (store.state.selectedCrag===null) || (store.state.selectedCrag===[]) )
                next('/projectOverview')
            } else {
                alert('Login to proceed')
                next('/login')
            }
        }
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
