import Vue from 'vue'
import App from './App.vue'
import router from "./router"
import axios from "axios"
import VueAxios from 'vue-axios'
import store from '@/store'
import vuetify from '@/plugins/vuetify'
import Vuelidate from "vuelidate"

Vue.use(Vuelidate)
Vue.use(VueAxios, axios)
Vue.config.productionTip = false

async function createVueApp() {
  await store.dispatch('loadLoggedInClimber')
  new Vue({
    store,
    vuetify,
    render: h => h(App),
    router
  }).$mount('#app')
}
createVueApp()
