package com.example.ProjectBuddy.repositories;

import com.example.ProjectBuddy.enums.CragType;
import com.example.ProjectBuddy.enums.Grading;
import com.example.ProjectBuddy.model.Route;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RouteCRUDRepository extends CrudRepository<Route, Integer> {
        //searching with a query
        @Query("SELECT r FROM Route r WHERE r.routeName LIKE %:routeName%")
        List<Route> searchByRouteNameLike(@Param("routeName") String routeName);

        //@Query("SELECT r FROM Route r WHERE r.countryName LIKE %:countryName%")
        List<Route> findByCragCountryCountryName(String selectedCountryName);

        List<Route> findByCragCragType(CragType cragType);

        List<Route> findByGrade(Grading selectedDifficulty);

        List<Route> findByRouteNameContaining(String routeName);

        List<Route> findByCrag(Integer id);

        List<Route> findByCragCragId(Integer cragId);

        List<Route> findAllByRouteId(int routeId);

}
