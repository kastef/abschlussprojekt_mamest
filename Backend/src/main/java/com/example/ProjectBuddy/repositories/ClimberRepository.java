package com.example.ProjectBuddy.repositories;

import com.example.ProjectBuddy.model.Climber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;

@Transactional
@Repository
public class ClimberRepository {

    @Autowired
    ClimberCRUDRepository climberCRUDRepository;

    public boolean insertNewClimber(Climber climber) {
        // extremely short way to write:
        //return climberCRUDRepository.findOneByEmail(email)!=null;
        climberCRUDRepository.save(climber);
        return true;
    }

}
