package com.example.ProjectBuddy.repositories;

import com.example.ProjectBuddy.model.Country;
import com.example.ProjectBuddy.model.Crag;
import com.example.ProjectBuddy.enums.CragType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional
public class CragRepository {

    @Autowired
    EntityManager em;

    @Autowired
    CragCRUDRepository cragCRUDRepository;

    //search by type boulder or route
    public Crag filterCragByType(CragType cragType)
    {
        for (Crag crag: cragCRUDRepository.findAll())
        {
         if (crag.getCragType().equals(cragType))
         {
             return crag;
         }
         else
         {
             continue;
         }
        }
        return null;
    }

    public void setCrag(String cragName, String cragTypeStr, Integer countryId) {
        try {
            Country country = em.find(Country.class, countryId);

            CragType cragType;

            if (cragTypeStr.equals("Route")) {
                cragType = CragType.ROUTE;
            } else if (cragTypeStr.equals("Boulder")) {
                cragType = CragType.BOULDER;
            } else {
                cragType = CragType.ROUTE;
            }

            Crag crag = new Crag(cragName, cragType, country);
            country.addCrag(crag);
            cragCRUDRepository.save(crag);
            em.flush();
        }  catch (Exception e) {
            System.err.println("Fehler in setCrag - NullPointer: " + e.getMessage());
        }
    }

}
