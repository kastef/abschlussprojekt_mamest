package com.example.ProjectBuddy.repositories;

import com.example.ProjectBuddy.model.SignUp;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SignUpCRUDRepository extends CrudRepository<SignUp, Integer> {

    //get comment on specific routes that was written by a climber
    List<SignUp> findByClimberClimberIdAndRouteRouteId(Integer climberId, Integer routeId);

    List<SignUp> findByClimberClimberId(Integer climberId);

    List<SignUp> findByRouteRouteIdOrderByCommentsDateTimeAsc(Integer routeId);

    List<SignUp> findByRouteCragCragId(Integer cragId);

    List<SignUp> findByRouteRouteIdAndFinishedDateIsNotNull(Integer routeId);

    /*@Modifying
    @Query("UPDATE Comment SET textfield = Where commentId=:id")
    SignUp findOneByCommentCommentId(@Param("id") int id ,@RequestBody );*/
}
