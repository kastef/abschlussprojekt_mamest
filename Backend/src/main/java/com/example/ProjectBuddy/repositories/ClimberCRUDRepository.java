package com.example.ProjectBuddy.repositories;

import com.example.ProjectBuddy.model.Climber;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClimberCRUDRepository extends CrudRepository<Climber,Integer> {

    List<Climber> findByLastname(String lastname);

    Climber findOneByEmail(String email);

    Climber findByEmailAndPassword(String email, String password);

}
