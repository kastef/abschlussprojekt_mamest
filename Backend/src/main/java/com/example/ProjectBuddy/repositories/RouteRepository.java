package com.example.ProjectBuddy.repositories;

import com.example.ProjectBuddy.enums.Grading;
import com.example.ProjectBuddy.model.Crag;
import com.example.ProjectBuddy.model.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional
public class RouteRepository {

    @Autowired
    EntityManager em;

    @Autowired
    RouteCRUDRepository routeCRUDRepository;

    public void setRoute(String routeName, Grading grade, Integer cragId) {
        try {
            Crag crag = em.find(Crag.class, cragId);

            Route route = new Route(routeName, grade, crag);
            crag.addRoute(route);
            routeCRUDRepository.save(route);
            em.flush();
        }  catch (Exception e) {
            System.err.println("Fehler in setRoute - NullPointer: " + e.getMessage());
        }
    }

}
