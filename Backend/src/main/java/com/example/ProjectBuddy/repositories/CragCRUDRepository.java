package com.example.ProjectBuddy.repositories;

import com.example.ProjectBuddy.model.Crag;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CragCRUDRepository extends CrudRepository<Crag, Integer> {

    //finding crags by name
    List<Crag> findByCragName(String cragName);

    //finding crags by countryname
    List<Crag> findByCountryCountryName(String selectedCountryName);

    //finding one crag by name
    Crag findOneByCragName(String cragName);

}
