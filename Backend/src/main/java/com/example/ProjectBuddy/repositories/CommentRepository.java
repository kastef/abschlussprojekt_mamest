package com.example.ProjectBuddy.repositories;

import com.example.ProjectBuddy.model.Comment;
import com.example.ProjectBuddy.model.SignUp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional
public class CommentRepository {

    @Autowired
    EntityManager em;

    @Autowired
    CommentCRUDRepository commentCRUDRepository;

    public void setComment(String textField, Integer signUpId) throws NullPointerException {
        try {
            SignUp signUp = em.find(SignUp.class, signUpId);

            Comment comment = new Comment(textField, signUp);
            commentCRUDRepository.save(comment);
            em.flush();
        } catch (Exception e) {
            System.err.println("Fehler in setComment - NullPointer: " + e.getMessage());
        }
    }
}