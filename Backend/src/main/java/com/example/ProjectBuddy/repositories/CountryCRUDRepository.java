package com.example.ProjectBuddy.repositories;

import com.example.ProjectBuddy.model.Country;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryCRUDRepository extends CrudRepository<Country, Integer> {

    //finding one country by name
    Country findOneByCountryName(String countryName);
}