package com.example.ProjectBuddy.repositories;

import com.example.ProjectBuddy.model.Climber;
import com.example.ProjectBuddy.model.Route;
import com.example.ProjectBuddy.model.SignUp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Transactional
@Repository
public class SignUpRepository {

    @Autowired
    EntityManager em;

    @Autowired
    SignUpCRUDRepository signUpCRUDRepository;

    public void setSignUp(Integer climberId, Integer routeId) throws NullPointerException {
        try {
            Climber climber = em.find(Climber.class, climberId);
            Route route = em.find(Route.class, routeId);
            SignUp signUp = new SignUp(climber, route);
            signUpCRUDRepository.save(signUp);
        } catch (Exception e) {
            System.err.println("Fehler in setSignUp - NullPointer: " + e.getMessage());
        }
    }

}
