package com.example.ProjectBuddy.repositories;

import com.example.ProjectBuddy.model.Comment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentCRUDRepository extends CrudRepository<Comment, Integer> {

    //todo route verknüpfen
    //finding one comment by signUpId
    Comment findOneBySignUpSignUpId(Integer signUpId);
    //finding comments by signUpId
    List<Comment> findBySignUpSignUpId(Integer signUpId);
    //finding comments by routeId
    List<Comment> findBySignUpRouteRouteId(Integer routeId);

}
