package com.example.ProjectBuddy.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ProjectBuddy.model.Crag;
import com.example.ProjectBuddy.repositories.CragCRUDRepository;

@Service
public class CragService {
    
    @Autowired
    EntityManager em;

    @Autowired
    CragCRUDRepository cragCRUDRepository;


    public List<String> allCragNames() {
        List<Crag> cragList = new ArrayList();
        cragCRUDRepository.findAll().iterator().forEachRemaining(cragList::add);

        List<String> cragNameList = new ArrayList<>();

        for (Crag crag : cragList) {
            cragNameList.add(crag.getCragName());
        }

        return cragNameList;
        //return (List<Crag>) cragCRUDRepository.findAll()
    }

    public Crag loadCragById (Integer cragId) {
        return cragCRUDRepository.findById(cragId).get();
    }
    
    public List<Crag> loadAllCrags() {
        return (List<Crag>) cragCRUDRepository.findAll();
    }

    public List<Crag> loadCragsByCountry (String selectedCountryName) {
        return cragCRUDRepository.findByCountryCountryName(selectedCountryName);
    }

    public List<Crag> cragSpecs(String cragName) {
        return cragCRUDRepository.findByCragName(cragName);
    }
}