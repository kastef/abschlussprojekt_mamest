package com.example.ProjectBuddy.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.example.ProjectBuddy.dtos.AvatarEditDTO;
import com.example.ProjectBuddy.dtos.EditClimberDTO;
import com.example.ProjectBuddy.dtos.RegisterDTO;
import com.example.ProjectBuddy.enums.Gender;
import com.example.ProjectBuddy.model.Climber;
import com.example.ProjectBuddy.repositories.ClimberCRUDRepository;
import com.example.ProjectBuddy.repositories.ClimberRepository;
import com.example.ProjectBuddy.security.MyUserDetailService;

@Service
public class ClimberService {
    
    @Autowired
    EntityManager entityManager;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    MyUserDetailService myUserDetailService;

    @Autowired
    ClimberRepository climberRepository;

    @Autowired
    ClimberCRUDRepository climberCRUDRepository;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    HttpServletRequest httpServletRequest;

    public Climber registerClimber(RegisterDTO registerDTO) {
        Gender gender = Gender.convertStringToEnum(registerDTO.getGenderAsString());

        if (climberCRUDRepository.findOneByEmail(registerDTO.getEmail()) != null)
            throw new ResponseStatusException(HttpStatus.CONFLICT, "E-Mail is taken.");

        Climber climber = new Climber(registerDTO.getFirstname(), registerDTO.getLastname(), gender, registerDTO.getDateOfBirth(), registerDTO.getEmail(), passwordEncoder.encode(registerDTO.getPassword()), registerDTO.getHasAcceptedToU());

        boolean hasRegistered = climberRepository.insertNewClimber(climber);
        System.out.println(hasRegistered);
        authWithAuthManager(httpServletRequest, registerDTO.getEmail(), registerDTO.getPassword());

        System.out.println(registerDTO.getLastname() + ", " + registerDTO.getFirstname() + " has successfully registered with: " + registerDTO.getEmail());
        climber.setPassword(null);
        return climber;
    }    

    //Authentifizierung im Backend
    private void authWithAuthManager(HttpServletRequest request, String email, String password) {
        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(email, password);
        authToken.setDetails(new WebAuthenticationDetails(request));
        Authentication authentication = authenticationManager.authenticate(authToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        request.getSession().setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext());

        System.out.println("authenticated Climber: " + email);
    }

    public Climber authenticateClimber() {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || authentication.getName().equals("anonymousUser")) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Sorry. Login failed.");
        } else {
            Climber climber = climberCRUDRepository.findOneByEmail(authentication.getName());
            if (climber != null)
                climber.setPassword(null);

            System.out.println(authentication.getName() + " has logged in");
            return climber;
        }
    }

    public Climber getActiveClimber() {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        Climber climber = climberCRUDRepository.findOneByEmail(authentication.getName());
        if (climber != null)
            climber.setPassword(null);
        return climber;
    }

    public void logout() {
        System.out.println(SecurityContextHolder.getContext().getAuthentication().getName() + " has logged out.");
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    public List<Climber> getAllClimbers() {
        var x = SecurityContextHolder.getContext();
        var climbers = climberCRUDRepository.findAll();
        List<Climber> climberList = new ArrayList<>();
        var iterator = climbers.iterator();
        while (iterator.hasNext()) {
            Climber climber = iterator.next();
            climber.setPassword(null);
            climberList.add(climber);
        }
        return climberList;
    }

    public void deleteAccount(Integer climberId) {
        if (climberCRUDRepository.existsById(climberId)) {
            Climber climber = climberCRUDRepository.findById(climberId).get();
            climber.deleteClimber(true, "deleted", "deleted", false);
            climberCRUDRepository.save(climber);
            SecurityContextHolder.getContext().setAuthentication(null);
            System.out.println("deleted Climber " + climber.getEmail());
        } else {
            System.out.println("delete was NOT possible");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "climber does not exist");
        }
    }

    public Climber editClimber(EditClimberDTO editedClimber) {
        Climber existingClimber = climberCRUDRepository.findById(editedClimber.getClimberId()).get();
        boolean hasNewPassword = false;
        try {
            // check if email is in database
            if (existingClimber.getEmail().equals(editedClimber.getEmail())) {
                // if yes skip
            } else {
                // if no do:
                Climber foundByMailClimber = climberCRUDRepository.findOneByEmail(editedClimber.getEmail());
                if (foundByMailClimber != null) {
                    throw new ResponseStatusException(HttpStatus.CONFLICT, "email already taken");
                }
                if (editedClimber.getEmail() != null) {
                    // reset the token
                    var authentication = SecurityContextHolder.getContext().getAuthentication();
                    existingClimber = climberCRUDRepository.findOneByEmail(authentication.getName());
                    if (editedClimber.getNewPassword() != null) {
                        var newAuthentication = new UsernamePasswordAuthenticationToken(editedClimber.getEmail(), editedClimber.getNewPassword());
                        SecurityContextHolder.getContext().setAuthentication(newAuthentication);
                        existingClimber.setPassword(passwordEncoder.encode(editedClimber.getNewPassword()));
                        hasNewPassword = true;
                    } else {
                        var newAuthentication = new UsernamePasswordAuthenticationToken(editedClimber.getEmail(), editedClimber.getPassword());
                        SecurityContextHolder.getContext().setAuthentication(newAuthentication);
                    }
                    existingClimber.setEmail(editedClimber.getEmail());
                }
            }
            if (editedClimber.getFirstname() != null) {
                existingClimber.setFirstname(editedClimber.getFirstname());
            }
            if (editedClimber.getLastname() != null) {
                existingClimber.setLastname(editedClimber.getLastname());
            }
            if (editedClimber.getGenderAsString() != null) {
                Gender gender = Gender.convertStringToEnum(editedClimber.getGenderAsString());
                existingClimber.setGender(gender);
            }
            if (editedClimber.getDateOfBirth() != null) {
                existingClimber.setDateOfBirth(editedClimber.getDateOfBirth());
                existingClimber.setAge(editedClimber.getAge());
            }
            if (editedClimber.getNewPassword() != null && !hasNewPassword) {
                existingClimber.setPassword(passwordEncoder.encode(editedClimber.getNewPassword()));
            }
            climberCRUDRepository.save(existingClimber);
            Climber climber = climberCRUDRepository.findByEmailAndPassword(existingClimber.getEmail(), existingClimber.getPassword());
            climber.setPassword(null);
            return climber;
        } catch (Exception e) {
            System.err.println("Something wrong with update User input " + e.getMessage());
        }
        return existingClimber;
    }

    public Climber editAvatar(AvatarEditDTO avatar) {
        if (!climberCRUDRepository.existsById(avatar.getClimberId())) {
            System.out.println("avatar edit was NOT possible");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "climber does not exist");
        }
        Climber climber = climberCRUDRepository.findById(avatar.getClimberId()).get();
        climber.setAvatarPath(avatar.getAvatarPath());
        climberCRUDRepository.save(climber);
        System.out.println("edited and saved avatar for " + climber.getEmail());
        climber.setPassword(null);
        return climber;
    }
}
