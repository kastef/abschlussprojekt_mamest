package com.example.ProjectBuddy.service;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ProjectBuddy.dtos.RouteNameDTO;
import com.example.ProjectBuddy.enums.CragType;
import com.example.ProjectBuddy.enums.Grading;
import com.example.ProjectBuddy.model.Route;
import com.example.ProjectBuddy.repositories.RouteCRUDRepository;
import com.example.ProjectBuddy.repositories.RouteRepository;

import static com.example.ProjectBuddy.enums.CragType.convertStringToEnum;

@Service
public class RouteService {
    
    @Autowired
    EntityManager em;

    @Autowired
    RouteCRUDRepository routeCRUDRepository;

    @Autowired
    RouteRepository routeRepository;

    public List<Route> getRoutes(RouteNameDTO route) {
        return routeCRUDRepository.searchByRouteNameLike(route.getRouteName());
    }

    public List<Route> getAllRoutes() {
        return (List<Route>) routeCRUDRepository.findAll();
    }

    public List<Route> getAllRoutesOfOneCrag(Integer cragId) {
        return routeCRUDRepository.findByCragCragId(cragId);
    }

    public List<Route> getRoutesByCountry(String selectedCountryName) {
        return routeCRUDRepository.findByCragCountryCountryName(selectedCountryName);
    }

    public List<Route> getRoutesByCragType(String selectedCragType) {
        CragType cragType = convertStringToEnum(selectedCragType);
        return routeCRUDRepository.findByCragCragType(cragType);
    }

    public List<Route> getRoutesByDifficulty(String selectedDifficulty) {
        return routeCRUDRepository.findByGrade(Grading.getGrading(selectedDifficulty));
    }

    public List<Route> getRouteById(Integer routeId){
        return routeCRUDRepository.findAllByRouteId(routeId);
    }
}