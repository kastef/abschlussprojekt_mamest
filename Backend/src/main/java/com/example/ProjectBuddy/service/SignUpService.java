package com.example.ProjectBuddy.service;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.example.ProjectBuddy.dtos.FinishedDateDTO;
import com.example.ProjectBuddy.dtos.SignUpDTO;
import com.example.ProjectBuddy.model.SignUp;
import com.example.ProjectBuddy.repositories.SignUpCRUDRepository;
import com.example.ProjectBuddy.repositories.SignUpRepository;

@Service
public class SignUpService {
 
    
    @Autowired
    EntityManager em;

    @Autowired
    SignUpRepository signUpRepository;

    @Autowired
    SignUpCRUDRepository signUpCRUDRepository;

    public void createSignUp(SignUpDTO signUpDTO) {
        signUpRepository.setSignUp(signUpDTO.getClimberId(), signUpDTO.getRouteId());
    }

    /* this is basically the same, but it should look different in the Frontend
    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST,path = "/api/enrollSignUp")
    public void addToSignUp(@RequestBody SignUpDTO signUpDTO) {
        signUpRepository.setSignUp(signUpDTO.getClimberId(), signUpDTO.getRouteId());
    }
    */

    /* get comment in specific routes that was written by a specific climber
     climber id should be here after the login
     !only a specific climber id with a specific route is going to be getted (comments are showed at routes)!*/

     public List<SignUp> getSignUpsByClimberIdAndRouteId(Integer climberId, Integer routeId) {
        return signUpCRUDRepository.findByClimberClimberIdAndRouteRouteId(climberId, routeId);
    }

    public List<SignUp> getSignUpsByClimberId(Integer climberId) {
        return signUpCRUDRepository.findByClimberClimberId(climberId);
    }

    public List<SignUp> getSignUpsByRouteIdAndFinishedDate(Integer routeId) {
        return signUpCRUDRepository.findByRouteRouteIdAndFinishedDateIsNotNull(routeId);
    }

    public List<SignUp> getSignUpsByRouteId(Integer routeId) {
        return signUpCRUDRepository.findByRouteRouteIdOrderByCommentsDateTimeAsc(routeId);
    }

    public List<SignUp> getAllSignUps() {
        return (List<SignUp>) signUpCRUDRepository.findAll();
    }

    public List<SignUp> getSignUpsByCrag(Integer cragId) {
        return (List<SignUp>) signUpCRUDRepository.findByRouteCragCragId(cragId);
    }

    public void deleteSignUp(Integer signUpId) throws ResponseStatusException {
        if (signUpCRUDRepository.existsById(signUpId)) {
            SignUp signUp = signUpCRUDRepository.findById(signUpId).get();
            signUp.deleteSignUp(true, null);
            signUpCRUDRepository.save(signUp);
            System.out.println("deleted signUp");
        } else {
            System.out.println("delete was not possible");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "signUp does not exist");
        }
    }

    public void putFinishedDate(FinishedDateDTO finishedDateDTO) throws ResponseStatusException {
        if (!signUpCRUDRepository.existsById(finishedDateDTO.getSignUpId())) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "signUp does not exits");
        }
        SignUp existingSignUp = signUpCRUDRepository.findById(finishedDateDTO.getSignUpId()).get();
        existingSignUp.setFinishedDate(finishedDateDTO.getFinishedDate());
        signUpCRUDRepository.save(existingSignUp);
    }
}