package com.example.ProjectBuddy.service;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ProjectBuddy.dtos.FilterCountryDTO;
import com.example.ProjectBuddy.model.Country;
import com.example.ProjectBuddy.model.Route;
import com.example.ProjectBuddy.repositories.CountryCRUDRepository;

@Service
public class CountryService {
    
    @Autowired
    EntityManager entityManager;

    @Autowired
    CountryCRUDRepository countryCRUDRepository;

    public List<Route> filterByCountry(FilterCountryDTO filterCountryDTO) {
        return null;
        
        //Country country = countryCRUDRepository.findOneByCountryName(filterCountryDTO.getCountryName());

        // this is working in H2 but not working in Java
        //SELECT route_Name, difficulty FROM Route JOIN Crag ON Crag.crag_Id = Route.crag JOIN Country ON CRAG.country = Country.country_Id WHERE country_Name='Italy'
        //Query query = entityManager.createQuery("SELECT Route FROM Route JOIN Crag ON Crag.cragId = Route.crag JOIN Country ON CRAG.country = Country.countryId WHERE countryName='" + filterCountryDTO.getCountryName() + "'");
        //List<Route> result = query.getResultList();
        //  System.out.println(result);
        //return result;
    }

    public List<Country> allCountries() {
        return (List<Country>) countryCRUDRepository.findAll();
    }   
}