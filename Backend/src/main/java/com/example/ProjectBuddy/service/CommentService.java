package com.example.ProjectBuddy.service;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ProjectBuddy.model.Comment;
import com.example.ProjectBuddy.repositories.CommentCRUDRepository;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Service
public class CommentService {

    @Autowired
    EntityManager entityManager;

    @Autowired
    CommentCRUDRepository commentCRUDRepository;
 
    public Comment getComment(Integer signUpId) {
        return commentCRUDRepository.findOneBySignUpSignUpId(signUpId);
    }

    public List<Comment> getComments(Integer signUpId) {
        return commentCRUDRepository.findBySignUpSignUpId(signUpId);
    }

    public Comment postComment(Integer signUp, String commentTextfieldDTO, Comment comment) throws UnsupportedEncodingException {
        String decoder = URLDecoder.decode(commentTextfieldDTO, StandardCharsets.UTF_8.toString());
        decoder = decoder.replace("=", " ");
        return commentCRUDRepository.save(new Comment(decoder,comment.getSignUp()));
    }

           /* @CrossOrigin
    @PostMapping(path = "/api/sendComment/{signUp}" , consumes = "application/x-www-form-urlencoded")
    public Comment postComment(@PathVariable int signUp,CommentTextfieldDTO commentTextfieldDTO, Comment comment) {
        return commentCRUDRepository.save(new Comment(commentTextfieldDTO.getTextfield(),comment.getSignUp()));
    }*/ //this one can be the solution
    //END OF COMMENT SECTION

    public List<Comment> allComments() {
        return (List<Comment>) commentCRUDRepository.findAll();
    }

    public List<Comment> getCommentsByRoute(Integer routeId) {
        return commentCRUDRepository.findBySignUpRouteRouteId(routeId);
    }
}