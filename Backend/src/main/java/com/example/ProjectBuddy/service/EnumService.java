package com.example.ProjectBuddy.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.ProjectBuddy.enums.*;

@Service
public class EnumService {
    
    public List<String> fetchGenders() {
        return Gender.getGendersAsList();
    }
    
    public List<String> fetchRoles() {
        return Role.getRolesAsList();
    }

    public List<String> fetchCragTypes() {
        return CragType.getCragTypesAsList();
    }

    public List<String> fetchGradings() { 
        return Grading.getGradingAsList();
    }
}