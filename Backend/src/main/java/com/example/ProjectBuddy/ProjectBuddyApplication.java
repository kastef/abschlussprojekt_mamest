package com.example.ProjectBuddy;

import com.example.ProjectBuddy.enums.Gender;
import com.example.ProjectBuddy.enums.Grading;
import com.example.ProjectBuddy.model.*;
import com.example.ProjectBuddy.repositories.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

@SpringBootApplication
public class ProjectBuddyApplication implements CommandLineRunner {

	Logger logger = LoggerFactory.getLogger(Logger.class);

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	ClimberCRUDRepository climberCRUDRepository;

	@Autowired
	CountryCRUDRepository countryCRUDRepository;

	@Autowired
	CragCRUDRepository cragCRUDRepository;

	@Autowired
	CragRepository cragRepository;

	@Autowired
	RouteRepository routeRepository;

	@Autowired
	SignUpRepository signUpRepository;

	@Autowired
	SignUpCRUDRepository signUpCRUDRepository;

	@Autowired
	CommentRepository commentRepository;

	public static void main(String[] args) {
		SpringApplication.run(ProjectBuddyApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		// CREATION of five dummy User -> Manuel, Mert, Stefan, Kathi and Conchita -> password for all 12345678
		String firstname = "Manuel";
		String lastname = "Lorenz";
		Gender gender = Gender.MALE;
		String birthDateAsString = "2000-01-29";
		LocalDate dateOfBirth = LocalDate.parse(birthDateAsString);
		String email = "manu.lo@mail.at";
		String password = "12345678";
		boolean hasAcceptedToU = true;

		try {
			logger.info(firstname);
			logger.info(lastname);
			logger.info(gender.toString());
			logger.info(dateOfBirth.toString());
			logger.info(email);
			logger.info(password);
			climberCRUDRepository.save(new Climber(firstname, lastname, gender, dateOfBirth, email, passwordEncoder.encode(password), hasAcceptedToU));
		} catch (Exception e) {
			System.err.println("Fehler beim Erstellen einer kletternden Person: " + e.getMessage());
		}

		Climber climber = climberCRUDRepository.findById(1).get();
		climber.setAvatarPath("https://avataaars.io/?avatarStyle=Circle?accessoriesType=Blank&clotheColor=Blue03&clotheType=ShirtVNeck&eyeType=Side&eyebrowType=UpDown&facialHairColor=Black&facialHairType=Blank&hairColor=Blonde&mouthType=Smile&skinColor=Pale&topType=ShortHairShortCurly");
		climberCRUDRepository.save(climber);

		firstname = "Mert";
		lastname = "Temel";
		gender = Gender.MALE;
		birthDateAsString = "2000-06-01";
		dateOfBirth = LocalDate.parse(birthDateAsString);
		email = "mert.temel@mail.at";

		try {
			logger.info(firstname);
			logger.info(lastname);
			logger.info(gender.toString());
			logger.info(dateOfBirth.toString());
			logger.info(email);
			logger.info(password);
			climberCRUDRepository.save(new Climber(firstname, lastname, gender, dateOfBirth, email, passwordEncoder.encode(password), hasAcceptedToU));
		} catch (Exception e) {
			System.err.println("Fehler beim Erstellen einer kletternden Person: " + e.getMessage());
		}

		climber = climberCRUDRepository.findById(2).get();
		climber.setAvatarPath("https://avataaars.io/?avatarStyle=Circle&topType=LongHairBun&accessoriesType=Round&hairColor=Black&facialHairType=BeardLight&facialHairColor=Black&clotheType=GraphicShirt&clotheColor=Black&graphicType=Skull&eyeType=Squint&eyebrowType=Default&mouthType=Serious&skinColor=Pale");
		climberCRUDRepository.save(climber);

		firstname = "Stefan";
		lastname = "Kaiser";
		gender = Gender.MALE;
		birthDateAsString = "1985-05-04";
		dateOfBirth = LocalDate.parse(birthDateAsString);
		email = "stef@mail.at";

		try {
			logger.info(firstname);
			logger.info(lastname);
			logger.info(gender.toString());
			logger.info(dateOfBirth.toString());
			logger.info(email);
			logger.info(password);
			climberCRUDRepository.save(new Climber(firstname, lastname, gender, dateOfBirth, email, passwordEncoder.encode(password), hasAcceptedToU));
		} catch (Exception e) {
			System.err.println("Fehler beim Erstellen einer kletternden Person: " + e.getMessage());
		}

		climber = climberCRUDRepository.findById(3).get();
		climber.setAvatarPath("https://avataaars.io/?avatarStyle=Circle&topType=NoHair&accessoriesType=Prescription02&facialHairType=BeardMedium&facialHairColor=Blonde&clotheType=Hoodie&clotheColor=Heather&eyeType=Default&eyebrowType=Default&mouthType=Default&skinColor=Tanned");
		climberCRUDRepository.save(climber);

		firstname = "Kathi";
		lastname = "Muhrer";
		gender = Gender.FEMALE;
		birthDateAsString = "1991-02-09";
		dateOfBirth = LocalDate.parse(birthDateAsString);
		email = "kathi@gmail.com";

		try {
			logger.info(firstname);
			logger.info(lastname);
			logger.info(gender.toString());
			logger.info(dateOfBirth.toString());
			logger.info(email);
			logger.info(password);
			climberCRUDRepository.save(new Climber(firstname, lastname, gender, dateOfBirth, email, passwordEncoder.encode(password), hasAcceptedToU));
		} catch (Exception e) {
			System.err.println("Fehler beim Erstellen einer kletternden Person: " + e.getMessage());
		}

		climber = climberCRUDRepository.findById(4).get();
		climber.setAvatarPath("https://avataaars.io/?avatarStyle=Circle?accessoriesType=Blank&clotheColor=Pink&clotheType=Hoodie&eyeType=Surprised&eyebrowType=DefaultNatural&facialHairColor=Brown&facialHairType=Blank&graphicType=Skull&hairColor=Blonde&mouthType=Smile&skinColor=Tanned&topType=LongHairFroBand");
		climberCRUDRepository.save(climber);

		firstname = "Conchita";
		lastname = "Wurst";
		gender = Gender.DIVERS;
		birthDateAsString = "1988-11-06";
		dateOfBirth = LocalDate.parse(birthDateAsString);
		email = "conchita@hotmail.com";

		try {
			logger.info(firstname);
			logger.info(lastname);
			logger.info(gender.toString());
			logger.info(dateOfBirth.toString());
			logger.info(email);
			logger.info(password);
			climberCRUDRepository.save(new Climber(firstname, lastname, gender, dateOfBirth, email, passwordEncoder.encode(password), hasAcceptedToU));
		} catch (Exception e) {
			System.err.println("Fehler beim Erstellen einer kletternden Person: " + e.getMessage());
		}

		climber = climberCRUDRepository.findById(5).get();
		climber.setAvatarPath("https://avataaars.io/?avatarStyle=Circle?accessoriesType=Blank&clotheColor=Pink&clotheType=CollarSweater&eyeType=Wink&eyebrowType=DefaultNatural&facialHairColor=Black&facialHairType=BeardLight&graphicType=Skull&hairColor=Black&mouthType=Twinkle&skinColor=Yellow&topType=LongHairStraight");
		climberCRUDRepository.save(climber);

		// with this .csv there is the problem that the separator is a normal, so if , was used in route names there was a mess in the data creation
		// use a substring elimination to get rid of the " sign for the cragTypes

		// use "src\\ALL_route_logbook.csv"; to put 2579 routes in the database
		// use "src\\route_logbook.csv"; for only putting 650 routes in the database -> much faster
		String fileName = "src\\route_logbook.csv";

		Set<String> countries = new HashSet<>();
		Set<String> cragTypes = new HashSet<>();	// two values for the ENUM
		Set<String> crags = new HashSet<>();
		List<String> routes = new ArrayList<>();

		BufferedReader reader = null;
		String line;
		int iteration = 0;

		Record record = new Record();
		try {
			reader = new BufferedReader(new FileReader(fileName));
			while ((line = reader.readLine()) != null) {
				if (iteration == 0) {
					iteration++;
					continue;
				}
				// each row is seen as one record -> object
				String[] row = line.split(";");
				setRecordValues(record, row[ColumnsCSV.COUNTRY.ordinal()], row[ColumnsCSV.CRAG_TYPE.ordinal()],
										row[ColumnsCSV.CRAG.ordinal()], row[ColumnsCSV.ROUTE.ordinal()],
										row[ColumnsCSV.DIFFICULTY.ordinal()]);
				// COUNTRY creation
				boolean hasFoundCountry = searchInDatabase(record.getCountryName(), countries);
				if (!hasFoundCountry) {
					createEntityInDatabase(record.getCountryName());
				}
				Country country = countryCRUDRepository.findOneByCountryName(record.getCountryName());
				// CRAG creation
				boolean hasFoundCrag = searchInDatabase(record.getCragName(), crags);
				if (!hasFoundCrag) {
					createEntityInDatabase(record.getCragName(), record.getCragType(), country);
				}
				Crag crag = cragCRUDRepository.findOneByCragName(record.getCragName());
				// ROUTE creation
				createEntityInDatabase(record.getRouteName(), Grading.getGrading(record.getDifficulty()), crag);

				addDatabaseEntriesToSetsAndList(countries, record.getCountryName(),
												cragTypes, record.getCragType(),
												crags, record.getCragName(),
												routes, record.getRouteName());
			}
		} catch (Exception e) {
			System.err.println("Fehler beim Durchsuchen der csv-file " + e.getMessage());
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				System.err.println("Reader konnte nicht geschlossen werden " + e.getMessage());
			}
		}

		// SIGNUP - creation
		signUpRepository.setSignUp(1, 123);
		signUpRepository.setSignUp(1, 32);
		signUpRepository.setSignUp(1, 644);
		signUpRepository.setSignUp(2, 32);
		signUpRepository.setSignUp(2, 123);
		signUpRepository.setSignUp(2, 587);
		signUpRepository.setSignUp(3, 123);
		signUpRepository.setSignUp(3, 587);
		signUpRepository.setSignUp(3, 644);

		// debug Sign Ups for Frontend
		//Route Name: "Como te canta el merequete"
		signUpRepository.setSignUp(1, 1);
		signUpRepository.setSignUp(2, 1);

		signUpRepository.setSignUp(4, 1);
		SignUp signUpTest1 = signUpCRUDRepository.findById(11).get();
		String finishedDateAsString1 = "2022-02-26";
		LocalDate finishedDate1 = LocalDate.parse(finishedDateAsString1);
        signUpTest1.setFinishedDate(finishedDate1);
        signUpCRUDRepository.save(signUpTest1);

		signUpRepository.setSignUp(5, 1);
		SignUp signUpTest2 = signUpCRUDRepository.findById(12).get();
		String finishedDateAsString2 = "2022-03-01";
		LocalDate finishedDate2 = LocalDate.parse(finishedDateAsString2);
        signUpTest2.setFinishedDate(finishedDate2);
        signUpCRUDRepository.save(signUpTest2);

		//Route Name: "Cortatus"
		signUpRepository.setSignUp(1, 2);
		SignUp signUpTest3 = signUpCRUDRepository.findById(13).get();
		String finishedDateAsString3 = "2022-03-05";
		LocalDate finishedDate3 = LocalDate.parse(finishedDateAsString3);
        signUpTest3.setFinishedDate(finishedDate3);
        signUpCRUDRepository.save(signUpTest3);

		signUpRepository.setSignUp(2, 2);
		SignUp signUpTest4 = signUpCRUDRepository.findById(14).get();
		String finishedDateAsString4 = "2022-03-04";
		LocalDate finishedDate4 = LocalDate.parse(finishedDateAsString4);
        signUpTest4.setFinishedDate(finishedDate4);
        signUpCRUDRepository.save(signUpTest4);

		signUpRepository.setSignUp(3, 2);

		//Route name: "Zafiro"
		signUpRepository.setSignUp(1, 3);
		signUpRepository.setSignUp(2, 3);
		signUpRepository.setSignUp(3, 3);

		// in crag Cueva Bollu
		signUpRepository.setSignUp(3, 12);
		signUpRepository.setSignUp(2, 12);
		signUpRepository.setSignUp(4, 12);
		signUpRepository.setSignUp(5, 12);
		//

		// COMMENT - creation
		// ordered by groups:
		// group 1 routeId 123
		commentRepository.setComment("Whatever, let's climb!!", 1);
		commentRepository.setComment("Let's go on Saturday", 5);
		commentRepository.setComment("I bin dabei!!", 7);
		commentRepository.setComment("Gemma, gemma!!", 7);
		// group 2 routeId 32
		commentRepository.setComment("Hallo Mert!", 2);
		commentRepository.setComment("Servas Manuel!", 4);
		// group 3 routeId 644
		commentRepository.setComment("Magst mit mir die Route probieren?", 9);
		commentRepository.setComment("Fix bin dabei!", 3);
		// group 3 routeId 587
		commentRepository.setComment("Sodala, da samma!!", 6);
		commentRepository.setComment("Jap, da samma. Wann hast du Zeit?", 8);
		commentRepository.setComment("Am Samstag ab 11 Uhr kann ich.", 6);
		commentRepository.setComment("11 passt super, ich hole dich bei der Tankstelle ab. Wie immer.", 8);
		commentRepository.setComment("Passt, werde dort sein. Bis dann!", 6);

		// group 4 routeId 1

		logger.info("Finished dummy data creation");
	}

	private class Record {
		private String countryName, cragType, cragName, routeName, difficulty;

		private String getCountryName() { return countryName; }
		private String getCragType() { return cragType; }
		private String getCragName() { return cragName; }
		private String getRouteName() { return routeName; }
		private String getDifficulty() { return difficulty; }

		private void setCountryName(String countryName) { this.countryName = countryName; }
		private void setCragType(String cragType) { this.cragType = cragType; }
		private void setCragName(String cragName) { this.cragName = cragName; }
		private void setRouteName(String routeName) { this.routeName = routeName; }
		private void setDifficulty(String difficulty) { this.difficulty = difficulty; }
	}

	private void setRecordValues(Record record, String countryName, String cragType, String cragName, String routeName, String routeDifficulty ) {
		record.setCountryName(countryName);
		record.setCragType(cragType.substring(1)); // to clean it form the " sign used substring(0)
		record.setCragName(cragName);
		record.setRouteName(routeName);
		record.setDifficulty(routeDifficulty);
	}

	private void addDatabaseEntriesToSetsAndList(Set<String> countries, String countryName,
												 Set<String> cragTypes, String cragTypeName,
												 Set<String> crags, String cragName,
												 List<String> routes, String routeName) {
		countries.add(countryName);
		cragTypes.add(cragTypeName);
		crags.add(cragName);
		routes.add(routeName);
	}

	private boolean searchInDatabase(String nameToSearch, Set<String> records) {
		boolean hasFound = false;
		for (String nameInSet : records) {
			if (nameInSet.equals(nameToSearch)) {
				hasFound = true;
				break;
			}
		}
		return hasFound;
	}

	private void createEntityInDatabase(String countryName) {
		try {
			logger.info(countryName);
			countryCRUDRepository.save(new Country(countryName));
		} catch (Exception e) {
			System.err.println("Fehler beim Einfügen eines Landes " + e.getMessage());
		}
	}

	private void createEntityInDatabase(String cragName, String cragType, Country country) {
		try {
			logger.info(cragName);
			cragRepository.setCrag(cragName, cragType, country.getCountryId());
		} catch (Exception e) {
			System.err.println("Fehler beim Einfügen eines Gebietes " + e.getMessage());
		}
	}

	private void createEntityInDatabase(String routeName, Grading grade, Crag crag) {
		try {
			logger.info(routeName);
			routeRepository.setRoute(routeName, grade, crag.getCragId());
		} catch (Exception e) {
			System.err.println("Fehler beim Einfügen einer Route " + e.getMessage());
		}
	}
	// CSV headline
	private enum ColumnsCSV {
		CRAG_TYPE,
		ROUTE,
		SECTOR,
		CRAG,
		COUNTRY,
		DATE,
		TYPE,
		RATING,
		TRIES,
		REPEAT,
		DIFFICULTY,
		STEEPNESS,
		PROTECTION,
		STYLE,
		COMMENT,
		HEIGHT,
		SITS,
		CHIPPED,
		NOTE,
		RECOMMENDED,
		FIRST_ASCENT,
		HARD,
		SECOND_GO,
		SOFT,
		TRADITIONAL,
		TRAVERSE,
		BOLTED_BY_ME,
		SAFETY;
	}

}

