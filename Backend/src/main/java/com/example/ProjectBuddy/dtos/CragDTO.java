package com.example.ProjectBuddy.dtos;

public class CragDTO {

    private String cragName;

    public String getCragName() {
        return cragName;
    }

    public void setCragName(String cragName) {
        this.cragName = cragName;
    }
}
