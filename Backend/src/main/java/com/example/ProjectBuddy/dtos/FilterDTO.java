package com.example.ProjectBuddy.dtos;

import com.example.ProjectBuddy.enums.CragType;

public class FilterDTO
{
    private String countryName ;
    private CragType cragType;
    private String routeDifficulty;

    public FilterDTO(String countryName, String inputedCragType, String routeDifficulty) {
        this.countryName = countryName;
        this.cragType = enumTransformation(inputedCragType);
        this.routeDifficulty = routeDifficulty;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public CragType getCragType() {
        return cragType;
    }

    public void setCragType(CragType cragType) {
        this.cragType = cragType;
    }

    public String getRouteDifficulty() {
        return routeDifficulty;
    }

    public void setRouteDifficulty(String routeDifficulty) {
        this.routeDifficulty = routeDifficulty;
    }
    private CragType enumTransformation(String inputedCragType)
    {
       if(inputedCragType.equalsIgnoreCase("BOULDER"))
        {
            return cragType = CragType.BOULDER;
        }
        else if (inputedCragType.equalsIgnoreCase("ROUTE"))
        {
            return cragType = CragType.ROUTE;
        }
        else
        {
            return null;
        }
    }
}
