package com.example.ProjectBuddy.dtos;

import java.time.LocalDate;

public class FinishedDateDTO {

    private Integer signUpId;
    private LocalDate finishedDate;

    public FinishedDateDTO(Integer signUpId, LocalDate finishedDate) {
        this.signUpId = signUpId;
        this.finishedDate = finishedDate;
    }

    public Integer getSignUpId() { return signUpId; }

    public LocalDate getFinishedDate() { return finishedDate; }

}
