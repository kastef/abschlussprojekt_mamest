package com.example.ProjectBuddy.dtos;

import java.time.LocalDate;

public class RegisterDTO
{
    private String firstname;
    private String lastname;
    private String inputGender;
    private LocalDate dateOfBirth;
    private String email;
    private String password;
    private Boolean hasAcceptedToU;

    public RegisterDTO(String firstname, String lastname, String inputGender, LocalDate dateOfBirth, String email, String password, Boolean hasAcceptedToU) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.inputGender = inputGender;
        this.dateOfBirth = dateOfBirth;
        this.email = email;
        this.password = password;
        this.hasAcceptedToU = hasAcceptedToU;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getGenderAsString() {
        return inputGender;
    }

    public void setGenderAsString(String inputGender) {
        this.inputGender = inputGender;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getHasAcceptedToU() {
        return hasAcceptedToU;
    }

    public void setHasAcceptedToU(Boolean hasAcceptedToU) {
        this.hasAcceptedToU = hasAcceptedToU;
    }

}
