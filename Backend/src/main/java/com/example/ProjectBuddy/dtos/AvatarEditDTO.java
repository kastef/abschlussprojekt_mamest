package com.example.ProjectBuddy.dtos;

public class AvatarEditDTO {

    private Integer climberId;
    private String avatarPath;

    public AvatarEditDTO(Integer climberId, String avatarPath) {
        this.climberId = climberId;
        this.avatarPath = avatarPath;
    }

    public Integer getClimberId() { return climberId; }

    public String getAvatarPath() { return avatarPath; }
    public void setAvatarPath(String avatarPath) { this.avatarPath = avatarPath; }
}
