package com.example.ProjectBuddy.dtos;

public class SignUpDTO {

    private Integer climberId;

    private Integer routeId;

    public SignUpDTO(Integer climberId, Integer routeId) {
        this.climberId = climberId;
        this.routeId = routeId;
    }

    public Integer getClimberId() { return climberId; }

    public Integer getRouteId() { return routeId; }

}

