package com.example.ProjectBuddy.dtos;

public class CommentTextfieldDTO
{
    private String textfield;

    public String getTextfield() {
        return textfield;
    }

    public void setTextfield(String textfield) {
        this.textfield = textfield;
    }

}
