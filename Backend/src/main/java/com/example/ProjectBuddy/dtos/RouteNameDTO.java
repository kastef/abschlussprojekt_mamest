package com.example.ProjectBuddy.dtos;

public class RouteNameDTO
{
    private String routeName;

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }
}
