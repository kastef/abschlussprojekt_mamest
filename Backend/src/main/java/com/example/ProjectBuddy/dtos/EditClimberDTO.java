package com.example.ProjectBuddy.dtos;

import java.time.LocalDate;
import java.time.Period;

public class EditClimberDTO {

    private Integer climberId;
    private String firstname;
    private String lastname;
    private String inputGender;
    private LocalDate dateOfBirth;
    private Integer age;
    private String email;
    private String password;
    private String newPassword;

    public EditClimberDTO(Integer climberId, String firstname, String lastname, String inputGender, LocalDate dateOfBirth, String email, String password, String newPassword) {
        this.climberId = climberId;
        this.firstname = firstname;
        this.lastname = lastname;
        this.inputGender = inputGender;
        this.dateOfBirth = dateOfBirth;
        if (dateOfBirth != null)
            this.age = calculateAge(dateOfBirth);
        this.email = email;
        this.password = password;
        this.newPassword = newPassword;
    }

    public Integer getClimberId() {
        return climberId;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getGenderAsString() {
        return inputGender;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public Integer getAge() {
        return age;
    }

    private Integer calculateAge(LocalDate dateOfBirth) {
        return Period.between(this.dateOfBirth, LocalDate.now()).getYears();
    }

    public String getEmail() { return email; }

    public String getPassword() { return password; }

    public String getNewPassword() { return newPassword; }


}
