package com.example.ProjectBuddy.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer commentId;

    @Column
    private LocalDateTime dateTime;

    @Column(nullable = false)
    private String textField;

    // relation to signup
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    @JoinColumn(name = "signUp")
    private SignUp signUp;

    public Comment() {}

    public Comment(String textField, SignUp signUp) {
        this.dateTime = LocalDateTime.now();
        this.textField = textField;
        this.signUp = signUp;
    }

    public Integer getCommentId() {
        return commentId;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }
    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getTextField() {
        return textField;
    }
    public void setTextField(String textField) {
        this.textField = textField;
    }

    @JsonBackReference
    public SignUp getSignUp() {
        return signUp;
    }
    public void setSignUp(SignUp signUp) {
        this.signUp = signUp;
    }
}
