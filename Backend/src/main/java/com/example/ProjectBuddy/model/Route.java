package com.example.ProjectBuddy.model;

import com.example.ProjectBuddy.enums.Grading;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Route {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer routeId;

    @Column
    private String routeName;

    @Enumerated
    private Grading grade;

    @Column
    private String difficulty;

    // relation to crag
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "crag")
    private Crag crag;

    // relation to route
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "route", cascade = CascadeType.DETACH)
    private Set<SignUp> signUps = new HashSet<>();

    // constructors
    public Route() {}

    public Route(String routeName, Grading grade, Crag crag) {
        this.routeName = routeName;
        this.grade = grade;
        this.difficulty = grade.getDifficulty();
        this.crag = crag;
    }

    // getter and setter
    public Integer getRouteId() {
        return routeId;
    }

    public String getRouteName() {
        return routeName;
    }
    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public Grading getGrade() {
        return grade;
    }
    public void setGrade(Grading grade) {
        this.grade = grade;
    }

    public String getDifficulty() { return difficulty; }

    public Crag getCrag() {
        return crag;
    }
    public void setCrag(Crag crag) {
        this.crag = crag;
    }

    @JsonIgnore
    public Set<SignUp> getSignUps() {
        return signUps;
    }
    public void addSignUp(SignUp signUp) {
        this.signUps.add(signUp);
    }
}
