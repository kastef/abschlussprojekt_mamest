package com.example.ProjectBuddy.model;

import com.example.ProjectBuddy.enums.CragType;
import com.fasterxml.jackson.annotation.JsonBackReference;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Crag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer cragId;

    @Column
    private String cragName;

    @Enumerated(EnumType.STRING)
    private CragType cragType;

    // relation to Country
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "country")
    private Country country;

    // relation to Route
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "crag", cascade = CascadeType.ALL)
    private List<Route> routes = new ArrayList<>();

    // constructors
    public Crag() {}

    public  Crag(String cragName, CragType cragType, Country country) {
        this.cragName = cragName;
        this.cragType = cragType;
        this.country = country;
    }

    // getter and setter
    public Integer getCragId() {
        return cragId;
    }

    public String getCragName() {
        return cragName;
    }
    public void setCragName(String cragName) {
        this.cragName = cragName;
    }

    public CragType getCragType() {
        return cragType;
    }
    public void setCragType(CragType cragType) {
        this.cragType = cragType;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @JsonBackReference
    public List<Route> getRoutes() {
        return routes;
    }
    public void addRoute(Route route) {
        this.routes.add(route);
    }
}
