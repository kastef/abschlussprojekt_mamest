package com.example.ProjectBuddy.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
public class SignUp {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer signUpId;

    @Column
    private LocalDate signUpDate;

    @Column
    private LocalDate finishedDate;

    @Column
    private boolean isDeleted;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "route")
    private Route route;

    //Climber relation
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "climber")
    private Climber climber;

    //Comment relation
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "signUp", cascade = CascadeType.ALL)
    private List<Comment> comments = new ArrayList<>();

    // constructors
    public SignUp() {}

    public SignUp(Climber climber, Route route) {
        this.signUpDate = LocalDate.now();
        this.climber = climber;
        this.route = route;
        this.finishedDate = null;
    }

    // getters and setters
    public Integer getSignUpId() {
        return signUpId;
    }

    public LocalDate getSignUpDate() {
        return signUpDate;
    }
    public void setSignUpDate(LocalDate signUpDate) {
        this.signUpDate = signUpDate;
    }

    public LocalDate getFinishedDate() {
        return finishedDate;
    }
    public void setFinishedDate(LocalDate finishedDate) {
        this.finishedDate = finishedDate;
    }

    public boolean isDeleted() { return isDeleted; }

    public void setDeleted(boolean deleted) { isDeleted = deleted; }

    public void deleteSignUp(boolean isDeleted, Climber climber) {
        this.setDeleted(isDeleted);
        this.setClimber(climber);
    }

    @JsonManagedReference
    public Route getRoute() {
        return route;
    }
    public void setRoute(Route route) {
        this.route = route;
    }

    @JsonManagedReference
    public Climber getClimber() { return climber; }
    public void setClimber(Climber climber) { this.climber = climber; }

    @JsonManagedReference
    public List<Comment> getComments() {
        return comments;
    }
    public void addComment(Comment comment) {
        this.comments.add(comment);
    }

    // TODO test this when Frontend is ready
    // idea to realise "Hall of Fame" entry
    // from Frontend a put to set the finishedDate
    // id finishedDate != null then...
    public String addToHallOfFame() {
        String hallOfFameEntry = null;
        if (this.finishedDate != null) {
            hallOfFameEntry = this.climber.getLastname() + ", " + this.climber.getFirstname();
            return hallOfFameEntry;
        }
        return hallOfFameEntry;
    }
    // can not test this without frontend
}
