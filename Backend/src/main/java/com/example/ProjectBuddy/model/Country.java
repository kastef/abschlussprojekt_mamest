package com.example.ProjectBuddy.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer countryId;

    @Column
    private String countryName;

    // relation to crags:
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "country", cascade = CascadeType.ALL)
    private Set<Crag> crags = new HashSet<>();

    // constructors:
    public Country() {}

    public Country(String countryName) {
        this.countryName = countryName;
    }

    // getter and setter:
    public Integer getCountryId() {
        return countryId;
    }

    public String getCountryName() {
        return countryName;
    }
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    @JsonBackReference
    public Set<Crag> getCrags() {
        return crags;
    }
    public void addCrag(Crag crag) {
        this.crags.add(crag);
    }

}
