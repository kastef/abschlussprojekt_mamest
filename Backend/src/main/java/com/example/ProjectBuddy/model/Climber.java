package com.example.ProjectBuddy.model;
import com.example.ProjectBuddy.enums.Gender;
import com.example.ProjectBuddy.enums.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Climber {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int climberId;

    @Enumerated(EnumType.STRING)
    private Role role;

    @Column(nullable = false)
    private String firstname;

    @Column(nullable = false)
    private String lastname;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    //@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    //@JsonFormat(pattern = "dd/MM/yyyy")
    @Column(length = 10) //falls Fehler enstehen. Kommentar ersetzen mit @DateTime und JsonFormat
    private LocalDate dateOfBirth;

    @Column
    private int age;

    @Column(unique = true)
    private String email;

    @Column
    private String password;

    @Column
    private boolean hasAcceptedToU;

    @Column(length = 2500)
    private String avatarPath;

    @Column
    private boolean isDeleted;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "climber", cascade = CascadeType.DETACH)
    @JsonIgnore
    private Set<SignUp> signUpSet = new HashSet<>();

    // constructors:
    public Climber() {}

    public Climber(String firstname, String lastname, Gender gender, LocalDate dateOfBirth, String email, String password, boolean hasAcceptedToU) {
        this.role = Role.CLIMBER;
        this.firstname = firstname;
        this.lastname = lastname;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.age = calculateAge(dateOfBirth);
        this.email = email;
        this.password = password;
        this.hasAcceptedToU = hasAcceptedToU;
    }

    private int calculateAge(LocalDate dateOfBirth) {
        return Period.between(this.dateOfBirth, LocalDate.now()).getYears();
    }

    //Getter && Setter
    public int getClimberId() {
        return climberId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getAge() { return age; }

    public void setAge(int age) { this.age = age; }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isHasAcceptedToU() {
        return hasAcceptedToU;
    }

    public void setHasAcceptedToU(boolean hasAcceptedToU) {
        this.hasAcceptedToU = hasAcceptedToU;
    }

    public String getAvatarPath() { return avatarPath; }

    public void setAvatarPath(String avatarPath) { this.avatarPath = avatarPath; }

    public boolean isDeleted() { return isDeleted; }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public void deleteClimber(boolean isDeleted, String firstname, String lastname, boolean hasAcceptedToU) {
        this.setDeleted(isDeleted);
        this.setFirstname(firstname);
        this.setLastname(lastname);
        this.setHasAcceptedToU(hasAcceptedToU);
    }

    public Set<SignUp> getSignUpSet() {
        return signUpSet;
    }

    public void setSignUpSet(SignUp signUp) {
        this.signUpSet.add(signUp);
    }
}
