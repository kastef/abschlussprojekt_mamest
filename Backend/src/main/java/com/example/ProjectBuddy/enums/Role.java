package com.example.ProjectBuddy.enums;

import java.util.ArrayList;
import java.util.List;

public enum Role {
    ADMIN,
    CLIMBER;

    // maybe we need this in the Frontend... so it is there to use
    public static List<String > getRolesAsList() {
        List<String> roleList = new ArrayList<>();
        for (Role role : Role.values()) {
            //System.out.println(role.toString());
            roleList.add(role.toString().toLowerCase());
        }
        return roleList;
    }

    public static Role convertStringToRole(String roleAsString) {
        Role role = null;
        switch (roleAsString.toLowerCase()) {
            case "admin": role = Role.ADMIN;
                break;
            case "climber": role = Role.CLIMBER;
                break;
        }
        return role;
    }

}
