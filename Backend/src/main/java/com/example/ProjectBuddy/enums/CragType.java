package com.example.ProjectBuddy.enums;

import java.util.ArrayList;
import java.util.List;

public enum CragType {
    BOULDER,
    ROUTE;

    public static List<String > getCragTypesAsList() {
        List<String> cragTypeList = new ArrayList<>();
        for (CragType cragType : CragType.values()) {
            cragTypeList.add(cragType.toString().toLowerCase());
        }
        return cragTypeList;
    }

    public static CragType convertStringToEnum(String cragTypeAsString) {
        CragType cragType = null;
        switch (cragTypeAsString.toLowerCase()) {
            case "route": cragType = CragType.ROUTE;
                break;
            case "boulder": cragType = CragType.BOULDER;
                break;
        }
        return cragType;
    }




}
