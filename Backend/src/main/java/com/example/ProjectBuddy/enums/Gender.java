package com.example.ProjectBuddy.enums;

import java.util.ArrayList;
import java.util.List;

public enum Gender {
    MALE,
    FEMALE,
    DIVERS;

    public static List<String > getGendersAsList() {
        List<String> genderList = new ArrayList<>();
        for (Gender gender : Gender.values()) {
            genderList.add(gender.toString().toLowerCase());
        }
        return genderList;
    }

    public static Gender convertStringToEnum(String genderAsString) {
        Gender gender = null;
        switch (genderAsString.toLowerCase()) {
            case "male": gender = Gender.MALE;
            break;
            case "female": gender = Gender.FEMALE;
            break;
            case "divers": gender = Gender.DIVERS;
            break;
        }
        return gender;
    }

}