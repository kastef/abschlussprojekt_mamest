package com.example.ProjectBuddy.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum Grading {

    GRADE_ROUTE_DREI_a("3a",false),
    GRADE_ROUTE_DREI_b("3b",false),
    GRADE_ROUTEE_DREI_c("3c",false),

    GRADE_ROUTE_VIER_a("4a",false),
    GRADE_ROUTE_VIER_b("4b",false),
    GRADE_ROUTE_VIER_c("4c",false),

    GRADE_ROUTE_FUENF_a("5a",false),
    GRADE_ROUTE_FUENF_a_PLUS("5a+",false),
    GRADE_ROUTE_FUENF_b("5b",false),
    GRADE_ROUTE_FUENF_b_PLUS("5b+",false),
    GRADE_ROUTE_FUENF_c("5c",false),
    GRADE_ROUTE_FUENF_c_PLUS("5c+",false),

    GRADE_ROUTE_SECHS_a("6a",false),
    GRADE_ROUTE_SECHS_a_PLUS("6a+",false),
    GRADE_ROUTE_SECHS_b("6b",false),
    GRADE_ROUTE_SECHS_b_PLUS("6b+",false),
    GRADE_ROUTE_SECHS_c("6c",false),
    GRADE_ROUTE_SECHS_c_PLUS("6c+",false),

    GRADE_ROUTE_SIEBEN_a("7a",false),
    GRADE_ROUTE_SIEBEN_a_PLUS("7a+",false),
    GRADE_ROUTE_SIEBEN_b("7b",false),
    GRADE_ROUTE_SIEBEN_b_PLUS("7b+",false),
    GRADE_ROUTE_SIEBEN_c("7c",false),
    GRADE_ROUTE_SIEBEN_c_PLUS("7c+",false),

    GRADE_ROUTE_ACHT_a("8a",false),
    GRADE_ROUTE_ACHT_a_PLUS("8a+",false),
    GRADE_ROUTE_ACHT_b("8b",false),
    GRADE_ROUTE_ACHT_b_PLUS("8b+",false),
    GRADE_ROUTE_ACHT_c("8c",false),
    GRADE_ROUTE_ACHT_c_PLUS("8c+",false),

    GRADE_ROUTE_NEUN_a("9a",false),
    GRADE_ROUTE_NEUN_a_PLUS("9a+",false),
    GRADE_ROUTE_NEUN_b("9b",false),
    GRADE_ROUTE_NEUN_b_PLUS("9b+",false),
    GRADE_ROUTE_NEUN_c("9c",false),

    GRADE_BOULDER_ZWEI("2",true),

    GRADE_BOULDER_DREI_A("3A",true),
    GRADE_BOULDER_DREI_B("3B",true),
    GRADE_BOULDER_DREI_C("3C",true),

    GRADE_BOULDER_VIER_A("4A",true),
    GRADE_BOULDER_VIER_B("4B",true),
    GRADE_BOULDER_VIER_C("4C",true),

    GRADE_BOULDER_FUENF_A("5A",true),
    GRADE_BOULDER_FUENF_B("5B",true),
    GRADE_BOULDER_FUENF_C("5C",true),

    GRADE_BOULDER_SECHS_A("6A",true),
    GRADE_BOULDER_SECHS_A_PLUS("6A+",true),
    GRADE_BOULDER_SECHS_B("6B",true),
    GRADE_BOULDER_SECHS_B_PLUS("6B+",true),
    GRADE_BOULDER_SECHS_C("6C",true),
    GRADE_BOULDER_SECHS_C_PLUS("6C+",true),

    GRADE_BOULDER_SIEBEN_A("7A",true),
    GRADE_BOULDER_SIEBEN_A_PLUS("7A+",true),
    GRADE_BOULDER_SIEBEN_B("7B",true),
    GRADE_BOULDER_SIEBEN_B_PLUS("7B+",true),
    GRADE_BOULDER_SIEBEN_C("7C",true),
    GRADE_BOULDER_SIEBEN_C_PLUS("7C+",true),

    GRADE_BOULDER_ACHT_A("8A",true),
    GRADE_BOULDER_ACHT_A_PLUS("8A+",true),
    GRADE_BOULDER_ACHT_B("8B",true),
    GRADE_BOULDER_ACHT_B_PLUS("8B+",true),
    GRADE_BOULDER_ACHT_C("8C",true),
    GRADE_BOULDER_ACHT_C_PLUS("8C+",true),

    GRADE_BOULDER_NEUN_A("9A",true);


    private final String difficulty;
    private final boolean isBoulder;
    private static Map<String, Grading> gradeMapByString;

    public static Grading getGrading(String grade) {
        return gradeMapByString.get(grade);
    }


    private Grading(String difficulty, boolean isBoulder) {
        this.difficulty = difficulty;
        this.isBoulder = isBoulder;
    }

    public String getDifficulty() { return difficulty; }
    public boolean isBoulder() { return isBoulder; }

    public static List<String > getGradingAsList() {
        List<String> gradingList = new ArrayList<>();
        for (Grading grading : Grading.values()) {
            //gradingList.add(grading.toString());
            gradingList.add(grading.difficulty);
        }
        return gradingList;
    }

    @Override
    public String toString() {
        return this.difficulty;
    }



    static {
        gradeMapByString = new HashMap<>();

        for(Grading x : Grading.values()) {
            gradeMapByString.put(x.difficulty, x);
            //System.out.println(x.difficulty);
            //System.out.println(x.toString());
        }
    }

}
