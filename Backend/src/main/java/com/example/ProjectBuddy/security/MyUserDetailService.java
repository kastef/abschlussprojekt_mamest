package com.example.ProjectBuddy.security;

import com.example.ProjectBuddy.model.Climber;
import com.example.ProjectBuddy.repositories.ClimberCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;

@Service
@Transactional
public class MyUserDetailService implements UserDetailsService {

    @Autowired
    private ClimberCRUDRepository climberCRUDRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Climber climber = climberCRUDRepository.findOneByEmail(email);  //find  Climber with email in our database (h2,mysql,)
        if(climber==null || climber.isDeleted()){
            throw new UsernameNotFoundException("No user found with username: " + email);
        }

        ArrayList<GrantedAuthority> authorities = new ArrayList<>();
        //hard coding a role for every user. (could also be retrieved from the database for advanced authority-management)
        authorities.add(new SimpleGrantedAuthority("ROLE_CLIMBER")); //Must be written with "ROLE_"-prefix (for reasons...)
        //authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));

        //Create spring security user with values from our userin
        User springSecUser = new User(climber.getEmail(),(climber.getPassword()),authorities);
        return springSecUser;
    }

    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return new BCryptPasswordEncoder(); //NoOpPasswordEncoder.getInstance();
    }
}