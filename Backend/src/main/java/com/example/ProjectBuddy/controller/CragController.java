package com.example.ProjectBuddy.controller;

import com.example.ProjectBuddy.model.Crag;
import com.example.ProjectBuddy.service.CragService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class CragController {

    @Autowired
    CragService cragService;

    @GetMapping(path="/allCragNames")
    public List<String> allCragNames() {
        return cragService.allCragNames();
    }

    @GetMapping(path = "/cragById/{cragId}")
    public Crag loadCragById (@PathVariable Integer cragId) {
        return cragService.loadCragById(cragId);
    }

    @GetMapping(path = "/cragsByCountry/{selectedCountryName}")
    public List<Crag> loadCragsByCountry (@PathVariable String selectedCountryName) {
        return cragService.loadCragsByCountry(selectedCountryName);
    }

    @GetMapping(path="/allCrags")
    public List<Crag> loadAllCrags() {
        return cragService.loadAllCrags();
    }

    @GetMapping(path = "/cragSpecs/{cragName}")
    public List<Crag> cragSpecs(@PathVariable String cragName) {
        return cragService.cragSpecs(cragName);
    }
}