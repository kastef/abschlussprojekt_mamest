package com.example.ProjectBuddy.controller;

import com.example.ProjectBuddy.dtos.RouteNameDTO;
import com.example.ProjectBuddy.model.Route;
import com.example.ProjectBuddy.service.RouteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class RouteController {

    @Autowired
    RouteService routeService;

    @CrossOrigin
    @PostMapping(path = "/routes")
    public List<Route> getRoutes(@RequestBody RouteNameDTO route) {
        return routeService.getRoutes(route);
    }

    @GetMapping(path="/allRoutes")
    public List<Route> allRoutes() {
        return routeService.getAllRoutes();
    }

    @GetMapping(path = "/allRoutesFromCrag/{cragId}")
    public List<Route> allRoutesOfOneCrag(@PathVariable Integer cragId) {
        return routeService.getAllRoutesOfOneCrag(cragId);
    }

    // this could also work by the id, but in the frontend we choose a string
    @GetMapping(path = "/routesByCountryName/{selectedCountryName}")
    public List<Route> loadRoutesByCountry(@PathVariable String selectedCountryName) {
        return routeService.getRoutesByCountry(selectedCountryName);
    }
    
    @GetMapping(path = "/routesByCragType/{selectedCragType}")
    public List<Route> loadRoutesByCragType(@PathVariable String selectedCragType) {
        return routeService.getRoutesByCragType(selectedCragType);
    }
    
    @GetMapping(path = "/routesByDifficulty/{selectedDifficulty}")
    public List<Route> loadRoutesByDifficulty(@PathVariable String selectedDifficulty) {
        return routeService.getRoutesByDifficulty(selectedDifficulty);
    }

    @GetMapping(path="/loadRouteIds/{routeId}")
    public List<Route> loadRouteIds(@PathVariable Integer routeId){
        return routeService.getRouteById(routeId);
    }
}