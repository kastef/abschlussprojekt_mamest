package com.example.ProjectBuddy.controller;

import com.example.ProjectBuddy.dtos.AvatarEditDTO;
import com.example.ProjectBuddy.dtos.EditClimberDTO;
import com.example.ProjectBuddy.dtos.RegisterDTO;
import com.example.ProjectBuddy.model.Climber;
import com.example.ProjectBuddy.service.ClimberService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class ClimberController {

    @Autowired
    ClimberService climberService;
    
    @PostMapping(path = "/register")
    public Climber register(@RequestBody RegisterDTO registerDTO) throws ResponseStatusException {
        return climberService.registerClimber(registerDTO);
    }

    @GetMapping(path = "/login")
    public Climber authenticateClimber() throws ResponseStatusException {
        return climberService.authenticateClimber();
    }

    @GetMapping(path = "/activeClimber")
    public Climber getActiveClimber() {
        return climberService.getActiveClimber();
    }

    @PostMapping(path = "/logout")
    public void logout() {
       climberService.logout();
    }

    @GetMapping(path = "/allClimbers")
    public List<Climber> getAllClimbers() {
        return climberService.getAllClimbers();
    }

    @DeleteMapping(path = "/api/deleteClimber/{climberId}")
    public void deleteAccount(@PathVariable("climberId") Integer climberId) throws ResponseStatusException {
        climberService.deleteAccount(climberId);
    }

    @PutMapping(path = "/editClimber")
    public Climber editClimber(@RequestBody EditClimberDTO editedClimber) throws ResponseStatusException {
        return climberService.editClimber(editedClimber);
    }

    @PutMapping(path = "/editAvatar")
    public Climber editAvatar(@RequestBody AvatarEditDTO avatar) throws IllegalStateException {
        return climberService.editAvatar(avatar);
    }
}