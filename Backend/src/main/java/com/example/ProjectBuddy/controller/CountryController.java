package com.example.ProjectBuddy.controller;

import com.example.ProjectBuddy.dtos.FilterCountryDTO;
import com.example.ProjectBuddy.model.Country;
import com.example.ProjectBuddy.model.Route;
import com.example.ProjectBuddy.repositories.*;
import com.example.ProjectBuddy.service.CountryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class CountryController {

    @Autowired
    EntityManager entityManager;

    @Autowired
    CountryCRUDRepository countryCRUDRepository;

    @Autowired
    CountryService countryService;


    @CrossOrigin
    @GetMapping(path = "/filterByCountry")
    @SuppressWarnings("unchecked")
    public List<Route> filterByCountry(@RequestBody FilterCountryDTO filterCountryDTO) throws Exception {
        // more refactoring needed...
        //Country country = countryCRUDRepository.findOneByCountryName(filterCountryDTO.getCountryName());

        // this is working in H2 but not working in Java
        //SELECT route_Name, difficulty FROM Route JOIN Crag ON Crag.crag_Id = Route.crag JOIN Country ON CRAG.country = Country.country_Id WHERE country_Name='Italy'
        Query query = entityManager.createQuery("SELECT Route FROM Route JOIN Crag ON Crag.cragId = Route.crag JOIN Country ON CRAG.country = Country.countryId WHERE countryName='"+filterCountryDTO.getCountryName()+"'");
        List<Route> result = query.getResultList();
        //  System.out.println(result);
        return result;
    }

    @GetMapping(path="/allCountries")
    public List<Country> allCountries() {
        return countryService.allCountries();
    }
}