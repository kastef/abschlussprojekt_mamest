package com.example.ProjectBuddy.controller;

import com.example.ProjectBuddy.service.EnumService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class EnumController {

    @Autowired
    EnumService enumService;

    @GetMapping(path = "/genders")
    public List<String> fetchGenders() {
        return enumService.fetchGenders();
    }

    @GetMapping(path = "/roles")
    public List<String> fetchRoles() {
        return enumService.fetchRoles();
    }

    @GetMapping(path = "/cragtypes")
    public List<String> fetchCragTypes() {
        return enumService.fetchCragTypes();
    }

    @GetMapping(path = "/grading")
    public List<String> fetchGradings() {
         return enumService.fetchGradings();
    }
}