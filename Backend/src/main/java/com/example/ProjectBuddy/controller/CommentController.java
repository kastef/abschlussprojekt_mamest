package com.example.ProjectBuddy.controller;

import com.example.ProjectBuddy.service.CommentService;

import com.example.ProjectBuddy.model.Comment;
import com.example.ProjectBuddy.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import java.io.UnsupportedEncodingException;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class CommentController {

    @Autowired
    EntityManager entityManager;

    @Autowired
    CommentCRUDRepository commentCRUDRepository;

    @Autowired
    CommentService commentService;
    
    @GetMapping(path="/comment/{signUpId}")
    public Comment getComment(@PathVariable Integer signUpId) {
        return commentService.getComment(signUpId);
    }

    @GetMapping(path="/comments/{signUpId}")
    public List<Comment> getComments(@PathVariable Integer signUpId) {
        return commentService.getComments(signUpId);
    }

    @PostMapping(path = "/sendComment/{signUp}", consumes = "application/x-www-form-urlencoded")
    public Comment postComment(@PathVariable int signUp, @RequestBody String commentTextfieldDTO, Comment comment)  throws UnsupportedEncodingException {
        return commentService.postComment(signUp, commentTextfieldDTO, comment);
    }

    @GetMapping(path="/allComments")
    public List<Comment> allComments() {
        return commentService.allComments();
    }

    @GetMapping(path="/commentsByRoute/{routeId}")
    public List<Comment> getCommentsByRoute(@PathVariable Integer routeId) {
        return commentCRUDRepository.findBySignUpRouteRouteId(routeId);
    }
}