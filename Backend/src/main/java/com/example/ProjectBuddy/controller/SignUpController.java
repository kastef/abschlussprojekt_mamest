package com.example.ProjectBuddy.controller;

import com.example.ProjectBuddy.dtos.FinishedDateDTO;
import com.example.ProjectBuddy.dtos.SignUpDTO;
import com.example.ProjectBuddy.model.SignUp;
import com.example.ProjectBuddy.repositories.SignUpCRUDRepository;
import com.example.ProjectBuddy.repositories.SignUpRepository;
import com.example.ProjectBuddy.service.SignUpService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityManager;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class SignUpController {

    @Autowired
    EntityManager em;

    @Autowired
    SignUpService signUpService;

    @Autowired
    SignUpRepository signUpRepository;

    @Autowired
    SignUpCRUDRepository signUpCRUDRepository;

    @CrossOrigin
    @PostMapping(path = "/createSignUp")
    public void createSignUp(@RequestBody SignUpDTO signUpDTO) {
        signUpService.createSignUp(signUpDTO);
    }
    
    @GetMapping(path="/signUpsByClimberIdAndRouteId/{climberId}/{routeId}")
    public List<SignUp> getSignUpsByClimberIdAndRouteId(@PathVariable Integer climberId, @PathVariable Integer routeId) {
        return signUpService.getSignUpsByClimberIdAndRouteId(climberId, routeId);
    }

    @GetMapping(path="/signUpsByClimberId/{climberId}")
    public List<SignUp> getSignUpsByClimberId(@PathVariable Integer climberId) {
        return signUpService.getSignUpsByClimberId(climberId);
    }

    @GetMapping(path="/signUpsByRouteIdAndFinishedDate/{routeId}")
    public List<SignUp> getSignUpsByRouteIdAndFinishedDate(@PathVariable Integer routeId) {
        return signUpService.getSignUpsByRouteIdAndFinishedDate(routeId);
    }

    //comments only for that route are getted
    @GetMapping(path="/signUpsByRouteId/{routeId}")
    public List<SignUp> getSignUpsByRouteId(@PathVariable Integer routeId) {
        return signUpService.getSignUpsByRouteId(routeId);
    }

    @GetMapping(path = "/allSignUps")
    public List<SignUp> getAllSignUps() {
        return signUpService.getAllSignUps();
    }

    @GetMapping(path = "/signUpsByCrag/{cragId}")
    public List<SignUp> getSignUpsByCrag(@PathVariable("cragId") Integer cragId) {
        return signUpService.getSignUpsByCrag(cragId);
    }

    @DeleteMapping(path = "/deleteSignUp/{signUpId}")
    public void deleteSignUp(@PathVariable("signUpId") Integer signUpId) throws ResponseStatusException {
       signUpService.deleteSignUp(signUpId);
    }

    @PutMapping(path = "/putFinishedDate")
    public void putFinishedDate(@RequestBody FinishedDateDTO finishedDateDTO) throws ResponseStatusException {
        signUpService.putFinishedDate(finishedDateDTO);
    }
}